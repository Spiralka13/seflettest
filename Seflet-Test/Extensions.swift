//
//  ViewController.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 02.06.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import UIKit

extension UIViewController { // ui stuff
    
    func subscribeToKeyboard() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    func keyboardWasShown (notification: NSNotification) {
        let info = notification.userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3, animations: {
                self.view.frame.origin.y = -keyboardSize.height
            })
        }
    }
    
    func keyboardWillBeHidden() {
        UIView.animateWithDuration(0.3, animations: {
            self.view.frame.origin.y = 0
            })
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    func addTapOutsideGestureRecognizer() {
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapOutSide.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapOutSide)
    }
    


}


