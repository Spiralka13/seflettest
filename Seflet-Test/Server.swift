//
//  Server.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 01.06.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import Alamofire

enum URL_PATHS: String {
    case base_url = "http://fashionfan.ru/"
    case login = "login"
    case people = "superstar"
}

class Server { //net requests
    static let makeRequest = Server()
    
    func login(username: String, password: String, completionHandler: (success: Bool) -> ()) {
        
        let url = URL_PATHS.base_url.rawValue + URL_PATHS.login.rawValue
        print("login url = \(url)")
        
        //http request here (alamofire for example)
        
        completionHandler(success: false)
    }
    
    func getFriends(page: Int, completionHandler: (nameOfArray: [Man], pages_count: Int, curent_page: Int) -> ()){
        let pages = ["page": page]
        Alamofire.request(.GET, URL_PATHS.base_url.rawValue + URL_PATHS.people.rawValue, parameters: pages, encoding: .URL, headers: nil).responseJSON { (answer: Response<AnyObject, NSError>) in
            if let jsonAnswer = answer.result.value as? NSDictionary {
                var currentArray: [Man] = []
                if let people = jsonAnswer["people"] as? [NSDictionary]! {
                    for item in people! {
                        currentArray.append(Man(json: item))
                    }
                }
                 let currentPage = jsonAnswer["current_page"] as! Int
                let pages_count = jsonAnswer["pages_count"] as! Int
                completionHandler(nameOfArray: currentArray, pages_count: pages_count, curent_page: currentPage)
            } else {
                completionHandler(nameOfArray: [], pages_count: 0, curent_page: 0)
            }
        }
        
    }
    
    
}
    
    
    
