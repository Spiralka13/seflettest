//
//  ViewController.swift
//  Seflet-Test
//
//  Created by Константин Черкашин on 21.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ViewController: UIViewController, iCarouselDelegate, iCarouselDataSource {
    
    @IBOutlet weak var imgForCor: UIImageView!
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var segmetnControl: AnimatedSegmentSwitch!
    
    
    let leftImg = UIImageView()
    let rightImg = UIImageView()
    var listOfFriends: [Man] = []
    
    var pages_count = 0
    var current_page = 1
    
    private lazy var statsSegmentSwitch: AnimatedSegmentSwitch = {

        let segmentControl = AnimatedSegmentSwitch()
        segmentControl.frame = CGRect(x: (self.view.bounds.width-self.view.bounds.width/2)/2 , y: 76, width: self.view.bounds.width/2, height: 30.0)
        segmentControl.autoresizingMask = [.FlexibleWidth]
        segmentControl.backgroundColor = UIColor(red: 6.0/255.0, green: 131.0/255.0, blue: 219.0/255.0, alpha: 1)
        segmentControl.selectedTitleColor = UIColor(red: 6.0/255.0, green: 131.0/255.0, blue: 219.0/255.0, alpha: 1)
        segmentControl.titleColor = .whiteColor()
        segmentControl.font = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
        segmentControl.thumbColor = .whiteColor()
        segmentControl.addTarget(self, action: "segmentValueDidChange:", forControlEvents: .ValueChanged)
        segmentControl.items = ["My City", "Near Me"]
        
        return segmentControl
    }()
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return listOfFriends.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width-self.view.bounds.width/5, height: (self.view.bounds.height/3)*2))
        tempView.backgroundColor = UIColor(red: 6.0/255.0, green: 131.0/255.0, blue: 219.0/255.0, alpha: 1)
        tempView.layer.cornerRadius = 10
        tempView.layer.shadowOffset = CGSize(width: 4, height: 4)
        tempView.layer.shadowOpacity = 0.6
        tempView.layer.shadowRadius = 3
        
        
        
        let nameLabel = UILabel(frame: CGRect(x: 10, y: tempView.bounds.height/3*2-tempView.bounds.height/8, width: (self.view.bounds.width-self.view.bounds.width/5)/3+80, height: (self.view.bounds.height/25)))
        nameLabel.text = listOfFriends[index].firstName + " " + listOfFriends[index].lastName
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.font = UIFont(name: "HelveticaNeue", size: 13.0)
        
        let cityAgeLabel = UILabel(frame: CGRect(x: 10, y: tempView.bounds.height/3*2-tempView.bounds.height/13, width: (self.view.bounds.width-self.view.bounds.width/5)/3+80, height: (self.view.bounds.height/25)))
        
        if listOfFriends[index].age != 0 && listOfFriends[index].city != "" {
            cityAgeLabel.text = listOfFriends[index].city + ", " + String(listOfFriends[index].age) + " Years Old"
        } else {
        if listOfFriends[index].age == 0 {
            cityAgeLabel.text = String(listOfFriends[index].city)
        }
        if listOfFriends[index].city == "" {
            cityAgeLabel.text = String(listOfFriends[index].age) + " Years Old"
        }
        }
        
        cityAgeLabel.textColor = UIColor.whiteColor()
        cityAgeLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 11.0)
        
        
        let tapToPhoto = UILabel(frame: CGRect(x: tempView.bounds.width/2-tempView.bounds.width/3, y: tempView.bounds.height-tempView.bounds.height/25*1.5, width: self.view.bounds.width-self.view.bounds.width/5, height: (self.view.bounds.height/25)))
        
        tapToPhoto.text = "Tap On Photo For More Details"
        tapToPhoto.textColor = UIColor(red: 0/255.0, green: 108.0/255.0, blue: 171.0/255.0, alpha: 1)
        tapToPhoto.font = UIFont(name: "HelveticaNeue", size: 12.0)

                
        let imgForCell = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width-self.view.bounds.width/5, height: (self.view.bounds.height/3)*2/3*2))
        imgForCell.contentMode = .ScaleAspectFill
        imgForCell.clipsToBounds = true
        imgForCell.sd_setImageWithURL(NSURL(string: listOfFriends[index].photo)!)
        let path = UIBezierPath(roundedRect:imgForCell.bounds, byRoundingCorners:[.TopRight, .TopLeft], cornerRadii: CGSize(width: 10, height:  10))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.CGPath
        imgForCell.layer.mask = maskLayer
        
        let likeButton = UIButton(frame: CGRect(x: 160, y: tempView.bounds.height/3*2-tempView.bounds.height/9, width: (self.view.bounds.width-self.view.bounds.width/5)/3-10, height: (self.view.bounds.height/35)*2-5))
        if listOfFriends[index].likesCount < 999 {
            likeButton.setTitle(" " + String(listOfFriends[index].likesCount), forState: .Normal)

        } else {
        likeButton.setTitle(String(listOfFriends[index].likesCount), forState: .Normal)
        }
        likeButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 12.0)
        likeButton.layer.borderWidth = 1
        likeButton.layer.borderColor = UIColor.whiteColor().CGColor
        likeButton.layer.cornerRadius = 14
        likeButton.setImage(UIImage(named: "Безымянный-7-8"), forState: .Normal)
        likeButton.addTarget(self, action: #selector(buttonAction), forControlEvents: .TouchUpInside)
        
        let addToFriends = UIButton(frame: CGRect(x: tempView.bounds.width/9, y: tempView.bounds.height/3*2+20, width: 50, height: 50 ))
        if listOfFriends[index].isYourFriend == false {
        addToFriends.setImage(UIImage(named: "Безымянный-7-9"), forState: .Normal)
        } else {
            addToFriends.setImage(UIImage(named: "Безымянный-7-15"), forState: .Normal)

        }
        let addToFriendLabel = UILabel(frame: CGRect(x: tempView.bounds.width/9-9, y: tempView.bounds.height/3*2+20+addToFriends.bounds.width/2*2, width: 100, height: 25 ))
        if listOfFriends[index].isYourFriend == true {
        addToFriendLabel.text = "Add To Friend"
        } else {
            addToFriendLabel.text = "Unsubscribe"
        }
        addToFriendLabel.textColor = UIColor.whiteColor()
        addToFriendLabel.font = UIFont(name: "HelveticaNeue", size: 10.0)
        
        let followButton = UIButton(frame: CGRect(x: tempView.bounds.width/2-25, y: tempView.bounds.height/3*2+20, width: 50, height: 50 ))
        followButton.setImage(UIImage(named: "Безымянный-7-10"), forState: .Normal)
        let followButtonLabel = UILabel(frame: CGRect(x: tempView.bounds.width/2, y: tempView.bounds.height/3*2+20+addToFriends.bounds.width/2*2, width: 100, height: 25 ))
        followButtonLabel.text = "Follow"
        followButtonLabel.textColor = UIColor.whiteColor()
        followButtonLabel.font = UIFont(name: "HelveticaNeue", size: 10.0)
        followButtonLabel.center.x = self.view.center.x+3
        
        let chatButton = UIButton(frame: CGRect(x: tempView.bounds.width*8/9-50, y: tempView.bounds.height/3*2+20, width: 50, height: 50 ))
        chatButton.setImage(UIImage(named: "Безымянный-7-11"), forState: .Normal)
        let chatButtonLabel = UILabel(frame: CGRect(x: tempView.bounds.width*8/9-45, y: tempView.bounds.height/3*2+20+addToFriends.bounds.width/2*2, width: 100, height: 25 ))
        chatButtonLabel.text = "Chat Him"
        chatButtonLabel.textColor = UIColor.whiteColor()
        chatButtonLabel.font = UIFont(name: "HelveticaNeue", size: 10.0)


        
        
        tempView.addSubview(imgForCell)
        tempView.addSubview(nameLabel)
        tempView.addSubview(tapToPhoto)
        tempView.addSubview(cityAgeLabel)
        tempView.addSubview(likeButton)
        tempView.addSubview(addToFriends)
        tempView.addSubview(followButton)
        tempView.addSubview(chatButton)
        tempView.addSubview(addToFriendLabel)
        tempView.addSubview(followButtonLabel)
        tempView.addSubview(chatButtonLabel)


        if index == carouselView.numberOfItems-3 {
        changeCurrentPage()
        }
        return tempView
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.type = .InvertedTimeMachine
        leftImg.frame = CGRect(x: self.view.bounds.width/6, y: 84, width: 16, height: 16)
        rightImg.frame = CGRect(x: self.view.bounds.width*5/6-16, y: 84, width: 16, height: 16)
        rightImg.image = UIImage(named: "Безымянный-7-6")
        leftImg.image = UIImage(named: "Безымянный-7-3")
        view.addSubview(leftImg)
        view.addSubview(rightImg)
        view.addSubview(statsSegmentSwitch)
        Server.makeRequest.getFriends(1) {(items: [Man], pages_count1: Int, current_page1: Int) in
            self.listOfFriends = items
            self.carouselView.reloadData()
            self.current_page = current_page1
            self.pages_count = pages_count1
            print (self.listOfFriends)
        }
        
        }

    func changeCurrentPage() {
        self.current_page += 1
        if current_page <= pages_count {
        Server.makeRequest.getFriends(self.current_page) { (nameOfArray, pages_count1, curent_page1) in
            
            for item in nameOfArray {
                self.listOfFriends.append(item)
            }
            
            self.carouselView.reloadData()
            self.current_page = curent_page1
            self.pages_count = pages_count1
            }
        }
    }
    
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.OffsetMultiplier {
            return 1.4
        }
        
        if option == iCarouselOption.ShowBackfaces {
            return 10
        }
        
        return value
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    
    
    func segmentValueDidChange(sender: AnimatedSegmentSwitch) {
        if sender.selectedIndex == 1{
            rightImg.image = UIImage(named: "Безымянный-7-4")
            leftImg.image = UIImage(named: "Безымянный-7-5")
        } else {
            rightImg.image = UIImage(named: "Безымянный-7-6")
            leftImg.image = UIImage(named: "Безымянный-7-3")
        }
        print("valueChanged: \(sender.selectedIndex)")
    }
    var check = 1
    var likes = 0
    func buttonAction(sender: UIButton!) {
        print("Button tapped")
        likes = Int((sender.titleLabel?.text)!)!
        if check%2 == 0{
            check += 1
        sender.setImage(UIImage(named: "Безымянный-7-8"), forState: .Normal)
        sender.setTitle("\(likes-1)", forState: .Normal)
        } else {
            check += 1
            sender.setImage(UIImage(named: "Безымянный-7-7"), forState: .Normal)
            sender.setTitle("\(likes+1)", forState: .Normal)

        }
    }
    
    
}







