//
//  Presenter.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 18.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import UIKit

class Presenter { // transitions between view controllers
    static let goTo = Presenter()
    
    func mainMenu() { //example
        UIApplication.sharedApplication().keyWindow?.rootViewController = UIStoryboard(name: "Menu", bundle: nil).instantiateInitialViewController()
    }
    
    
}