//
//  Man.swift
//  HelooUltraPro
//
//  Created by Константин Черкашин on 20.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation

enum ManFields : String { // keys for server's responses
    case firstName = "firstName"
    case lastName = "lastName"
    case likesCount = "likesCount"
    case isYourFriend = "isYourFriend"
    case age = "age"
    case city = "city"
    case photo = "photo"
    
}

class Man: NSObject { // model of user
    private(set) var firstName: String = ""
    private(set) var lastName: String = ""
    private(set) var likesCount: Int = 0
    private(set) var isYourFriend: Bool = false
    private(set) var city: String = ""
    private(set) var photo: String = ""
    private(set) var age: Int = 0
    
    init(json: NSDictionary) {
        
        if let someConstant = json[ManFields.firstName.rawValue] as? String {
            self.firstName = someConstant
        }
        
        if let someConstant = json[ManFields.lastName.rawValue] as? String {
            self.lastName = someConstant
        }
        
        if let someConstant = json[ManFields.likesCount.rawValue] as? Int {
            self.likesCount = someConstant
        }
        
        if let someConstant = json[ManFields.isYourFriend.rawValue] as? Bool {
            self.isYourFriend = someConstant
        }
        
        if let someConstant = json[ManFields.city.rawValue] as? String {
            self.city = someConstant
        }
        
        if let someConstant = json[ManFields.photo.rawValue] as? String {
            self.photo = someConstant
        }
        
        if let someConstant = json[ManFields.age.rawValue] as? Int {
            self.age = someConstant
        }
    
        
    }
    
}